TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle qt
QMAKE_CXXFLAGS += -fopenmp
QMAKE_LFLAGS += -fopenmp

SOURCES += main.cpp \
    setcover.cpp

HEADERS += \
    setcover.h
