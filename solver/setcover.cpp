#include "setcover.h"
#include <omp.h>

bool SetCover::read_csv(string filename)
{
    // verify header
    vector<string> header = {"RUNID", "MODULE_NAME", "LINENO", "COUNT"};
    const int size = header.size();

    ifstream fp(filename);
    string first;
    getline(fp, first);
    char h_buf[max_csv_line + 1];
    char* h_ptr[max_csv_cols + 1];
    if (first.size() > max_csv_line ||
        _csv_parse(first.c_str(), h_buf, h_ptr) != size)
        return false;
    for (int i = 0; i < size; ++i)
        if (h_ptr[i] != header[i])
            return false;
    streampos start = fp.tellg();
    fp.close();

    // processing data
    unordered_map<string,int> module_map;
    unordered_map<module_line,int> column_map;
    unordered_map<int,int> row_map;
    vector<int> column_data;
    vector<int> row_data;

    // restore maps from state (support for multiple files)
    for (int i = 0, n = module_list.size(); i < n; i++)
        module_map[module_list[i]] = i;
    for (int i = 0, n = column_list.size(); i < n; i++)
        column_map[column_list[i]] = i;
    for (int i = 0, n = row_list.size(); i < n; i++)
        row_map[row_list[i]] = i;

#pragma omp parallel num_threads(2)
{
    // build column/row indexes in parallel
    int thread_id = omp_get_thread_num();
    string line;
    char csv_line[max_csv_line + 1];
    char* csv_item[max_csv_cols + 1];

    // read CSV content
    ifstream in(filename);
    in.seekg(start, in.beg);
    while (getline(in, line)) {
        if (line.size() > max_csv_line ||
            _csv_parse(line.c_str(), csv_line, csv_item) != size)
            continue;  // skip invalid lines

        if (thread_id) {
            // find module index
            int m_idx;
            string name(csv_item[1]);
            auto m_ref = module_map.find(name);
            if (m_ref != module_map.end()) {
                m_idx = m_ref->second;
            } else {
                m_idx = module_map[name] = module_list.size();
                module_list.push_back(name);
            }
            // find column index
            int c_idx;
            module_line key = ((module_line)m_idx << 32) | atoi(csv_item[2]);
            auto c_ref = column_map.find(key);
            if (c_ref != column_map.end()) {
                c_idx = c_ref->second;
            } else {
                c_idx = column_map[key] = column_list.size();
                column_list.push_back(key);
            }
            // append to column list
            if (atoi(csv_item[3]) > 0)
                column_data.push_back(c_idx);
        } else {
            // find row index
            int r_idx;
            int runid = atoi(csv_item[0]);
            auto r_ref = row_map.find(runid);
            if (r_ref != row_map.end()) {
                r_idx = r_ref->second;
            } else {
                r_idx = row_map[runid] = row_list.size();
                row_list.push_back(runid);
            }
            // append to row list
            if (atoi(csv_item[3]) > 0)
                row_data.push_back(r_idx);
        }
    }
}

    // release objects
    unordered_map<string,int>().swap(module_map);
    unordered_map<module_line,int>().swap(column_map);
    unordered_map<int,int>().swap(row_map);

    // prepare sparse matrix
    n_rows = row_list.size();
    n_cols = column_list.size();
    test_set.resize(n_rows);
    vector<bool> cover(debug ? n_cols : 0);

#pragma omp parallel
{
    // build sparse matrix in parallel
    int n_threads = omp_get_num_threads();
    int thread_id = omp_get_thread_num();

    for (int i = 0, n = row_data.size(); i < n; i++) {
        int row_id = row_data[i], col_id = column_data[i];
        if (row_id % n_threads == thread_id)
            test_set[row_id].push_back(col_id);
        if (debug && !thread_id)
            cover[col_id] = 1;
    }

    // sort and remove duplicate values
    for (int i = thread_id; i < n_rows; i += n_threads) {
        vector<int>& data = test_set[i];
        sort(data.begin(), data.end());
        int src = 0, dst = 0, n = data.size();
        for (; src < n; src++) {
            int col_id = data[src];
            if (!dst || data[dst - 1] != col_id)
                data[dst++] = col_id;
        }
        data.resize(dst);
    }
}

    // print file stats
    if (debug) {
        int cover_sum = 0;
        for (auto it : cover)
            cover_sum += it;
        cout << "non-empty lines: " << row_data.size() << endl;
        cout << "rows (runid): " << n_rows << endl;
        cout << "columns (module/line): " << n_cols << endl;
        cout << "max.coverage: " << (double)cover_sum / n_cols << endl;
    }
    return true;
}

vector<int> SetCover::calculate_rank()
{
    // process all rows (tests)
    vector<int> result;
    vector<int> rows(n_rows);
    for (int i = 0; i < n_rows; i++)
        rows[i] = i;
    vector<bool> cover(n_cols);
    vector<int> gain(n_rows);

    while (rows.size()) {
        // calculate mask difference
        int n = rows.size();
        #pragma omp parallel
        {
            #pragma omp for
            for (int i = 0; i < n; i++) {
                int score = 0;
                for (auto it : test_set[rows[i]])
                    if (!cover[it])
                        score++;
                gain[i] = score;
            }
        }

        // find best improvement
        int best_pos = 0, best_val = 0;
        vector<int> remove;
        for (int i = 0; i < n; i++) {
            if (gain[i] > best_val) {
                best_pos = rows[i];
                best_val = gain[i];
            } else if (!gain[i])
                remove.push_back(rows[i]);
        }
        if (!best_val)
            break;  // no improvement possible

        // update result
        result.push_back(best_pos);
        for (auto it : test_set[best_pos])
            cover[it] = 1;
        if (debug)
            cout << "runid=" << row_list[best_pos]
                 << ", gain=" << best_val
                 << ", rows=" << rows.size() << endl;

        // build sorted list of remove positions
        vector<int>::iterator insert_pos = remove.begin();
        for (; insert_pos != remove.end(); insert_pos++)
            if (*insert_pos > best_pos)
                break;
        remove.insert(insert_pos, best_pos);
        remove.push_back(n_rows);  // end marker

        // rebuild rows array
        vector<int> valid_rows;
        int p = 0;
        for (auto it : rows) {
            if (it != remove[p])
                valid_rows.push_back(it);
            else
                p++;
        }
        rows.swap(valid_rows);
    }
    return result;
}

vector<int> SetCover::result_plain(const vector<int> &rank)
{
    vector<int> result;
    for (auto it : rank)
        result.push_back(row_list[it]);
    return result;
}

vector<pair<int,int>> SetCover::result_count(const vector<int> &rank)
{
    vector<pair<int,int>> result;
    vector<bool> cover(n_cols);
    for (auto it : rank) {
        int count = 0;
        for (auto cid : test_set[it])
            if (!cover[cid]) {
                cover[cid] = 1;
                count++;
            }
        result.push_back(make_pair(row_list[it], count));
    }
    return result;
}

vector<pair<int,vector<string>>> SetCover::result_cover(const vector<int> &rank)
{
    vector<pair<int,vector<string>>> result;
    vector<bool> cover(n_cols);
    for (auto it : rank) {
        vector<string> names;
        for (auto cid : test_set[it])
            if (!cover[cid]) {
                cover[cid] = 1;
                names.push_back(get_name(column_list[cid]));
            }
        result.push_back(make_pair(row_list[it], names));
    }
    return result;
}

string SetCover::get_name(module_line id)
{
    int m_idx = id >> 32, line_no = id & 0xffffffff;
    stringstream ss;
    ss << module_list[m_idx] << ':' << line_no;
    return ss.str();
}

int SetCover::_csv_parse(const char *str, char *buf, char **ptr)
{
    const char* src = str;
    char* dst = buf;
    int n = 0;
    ptr[n++] = dst;
    while (char ch = *src++) {
        if (ch == csv_separator) {
            *dst++ = '\0';
            ptr[n++] = dst;
            if (n == max_csv_cols) break;
        } else
            *dst++ = ch;
    }
    if (*str && dst[-1] == '\r') --dst;
    *dst++ = '\0';
    ptr[n] = dst;
    for (int i = 0; i < n; i++) {
        char* p1 = ptr[i];
        char* p2 = ptr[i + 1] - 2;
        if (p1 < p2 && *p1 == csv_quote && *p2 == csv_quote) {
            ptr[i]++;
            *p2 = '\0';
        }
    }
    return n;
}
