#ifndef SETCOVER_H
#define SETCOVER_H

#include <bits/stdc++.h>
using namespace std;

class SetCover
{
public:
    SetCover(bool debug=false)
        : debug(debug), n_rows(0), n_cols(0) { }
    virtual ~SetCover() { }

    bool read_csv(string filename);
    vector<int> calculate_rank();
    vector<int> result_plain(const vector<int>& rank);
    vector<pair<int,int>> result_count(const vector<int>& rank);
    vector<pair<int,vector<string>>> result_cover(const vector<int>& rank);

    int rows_count() { return n_rows; }
    int cols_count() { return n_cols; }

    static const int max_csv_line = 1000;
    static const int max_csv_cols = 10;
    static const char csv_separator = ';';
    static const char csv_quote = '"';

protected:
    typedef uint64_t module_line;
    string get_name(module_line id);

    bool debug;
    int n_rows, n_cols;
    vector<string> module_list;
    vector<module_line> column_list;
    vector<int> row_list;
    vector<vector<int>> test_set;

private:
    int _csv_parse(const char* str, char* buf, char** ptr);
};

#endif // SETCOVER_H
