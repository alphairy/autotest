#include "setcover.h"

int main(int argc, char** argv)
{
    vector<string> files;
    bool help = false, debug = false,
         print_size = false, print_count = false, print_cover = false;
    for (int i = 1; i < argc; i++) {
        string arg(argv[i]);
        if (arg.substr(0, 2) == "--") {
            if (arg == "--help")
                help = true;
            else if (arg == "--debug")
                debug = true;
            else if (arg == "--print-size")
                print_size = true;
            else if (arg == "--print-count")
                print_count = true;
            else if (arg == "--print-cover")
                print_cover = true;
        } else {
            files.push_back(arg);
        }
    }
    if (!files.size() || help) {
        cout << "Usage: autotest [OPTION]... [FILE]..." << endl;
        cout << "  --help   display this help and exit" << endl;
        cout << "  --debug  show debugging information" << endl;
        cout << "  --print-size   display dimensions" << endl;
        cout << "  --print-count  short coverage info" << endl;
        cout << "  --print-cover  full coverage info" << endl;
        return 1;
    }

    SetCover data(debug);
    for (auto fn : files) {
        if (!data.read_csv(fn)) {
            cerr << "Invalid CSV file: " << fn << endl;
            continue;
        }
    }
    if (print_size) {
        cout << data.rows_count() << " " << data.cols_count() << endl;
    }

    auto rank = data.calculate_rank();
    if (print_cover) {
        for (auto it : data.result_cover(rank)) {
            cout << it.first;
            for (auto name : it.second)
                cout << ' ' << name;
            cout << endl;
        }
    } else if (print_count) {
        for (auto it : data.result_count(rank))
            cout << it.first << ' ' << it.second << endl;
    } else {
        for (auto it : data.result_plain(rank))
            cout << it << endl;
    }
    return 0;
}
