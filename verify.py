
import pandas as pd
import sys


def verify_short(X, sol):
    cover = {}
    for k, g in X.groupby(['RUNID']):
        cover[int(k)] = set(g['SOURCE'].values)
    cover_all = set()
    for c in cover.values():
        cover_all.update(c)
    for k in sol:
        cover_all -= cover.get(int(k), set())
    if cover_all:
        for src in sorted(cover_all):
            print(src)
        print("not covered: %s" % len(cover_all))
    else:
        print("full coverage")


if __name__ == '__main__':
    if len(sys.argv) > 2:
        X = pd.read_csv(sys.argv[1], sep=';')
        X = pd.DataFrame(X[X['COUNT'] > 0])
        X['SOURCE'] = ['%s:%s' % x for x in zip(X['MODULE_NAME'].values, X['LINENO'].values)]
        sol = [line.rstrip() for line in open(sys.argv[2])]
        verify_short(X, sol)
