
from flask import Flask

# initialize app
app = Flask(__name__)
app.config.from_pyfile('config.py')
