import os
import subprocess

from flask import request, jsonify, render_template
from web.app import app


def get_prj_dir():
    return os.path.abspath(".")

def get_test_data_dir():
    return os.path.join(get_prj_dir(), app.config["TEST_DATA_DIR"])


@app.route('/')
def main():
    """ display main page.
    """
    file_names = sorted([fname for fname in os.listdir(get_test_data_dir()) if fname.endswith('.csv')])
    return render_template('main.html', file_names=file_names)

@app.route('/calc', methods=["POST"])
def calc():
    accumulations = []
    post = request.form
    if post:
        fname = post.get("filename")
        if not fname:
            return jsonify([])
        stdout, err = process(fname)
        out = stdout.split(b'\n')
        run_ids_total, lines_total = out[0].split(b' ')
        counts = [entry.split(b' ') for entry in out[1:] if entry]
        sum_ = 0
        for entry in counts:
            sum_ += int(entry[1])
            accumulations.append((entry[0], sum_))

        accumulations.insert(0, int(run_ids_total))
        accumulations.insert(0, int(lines_total))

    return jsonify(accumulations)



def process(fname):
    cmd = [
        os.path.join(get_prj_dir(), 'autotest'),
        os.path.join(get_test_data_dir(), fname),
        "--print-size",
        "--print-count",
    ]
    stdout, error_code = _run_command(cmd)
    return stdout, error_code


def _run_command(command):

    p = subprocess.Popen(command,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE,
                         )
    stdout, stderr = p.communicate()
    error_code = p.returncode

    if error_code:
        raise subprocess.CalledProcessError(
            error_code, command)

    return stdout, error_code


if __name__ == '__main__':
    app.run()
